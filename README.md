# Lab9 - Rollbacks and reliability

## Introduction

Probably you've already worked with reliability at Distributed Systems course. So today we are going to talk about rollback implementation in software.(Though I think you've worked with it as well, but maybe don't know about it) ***Let's roll!***

## Rollbacks

If you have a very complicated or very important function that you need to execute it is a good thing to implement Rollbacks there. You are basically saving the state of the execution, and you are returning to it in case of faliure on the next execution state, to try execute next step once again or return the results of at least previous step. One of the most used areas is databases, when you have a chunk of requests and you want all of them to pass, or none of them, so you are sending them in one commit, and if failure happens, your changes from commit are being removed.

## Lab

Ok, let's crush all the stuff out of it:
1. Create your fork of the `
Lab9 - Rollbacks and reliability
` repo and clone it. [***Here***](https://gitlab.com/sqr-inno/lab8-rollbacks-s22)
2. We are going to write some code today, our task is to make some backend for in-game shop. Instances that we should manage as follows: players (their names and balance), shop itself (limited only to list of products, their price and quantity in stock) plus for homework your task will be to implement inventory of a player. We will be storing all our data in databse, lets use for example postgresql.
+ Install postgresql(If you have Ubuntu/Mint you should already have it installed)
+ Now, let's open postgresql and create DB like this:
```sql
CREATE DATABASE lab;
\c lab
CREATE TABLE Player (username TEXT UNIQUE, balance INT CHECK(balance >= 0));
CREATE TABLE Shop (product TEXT UNIQUE, in_stock INT CHECK(in_stock >= 0), price INT CHECK (price >= 0));
```
+ Pay attention to constraints that we have for created table. Player balance, price and amount of product in stock cannot be negative. 
+ Ofcourse for testing we need some sample data, lets fill in tables with it:
```
INSERT INTO Player (username, balance) VALUES ('Alice', 100);
INSERT INTO PLayer (username, balance) VALUES ('Bob', 200);

INSERT INTO Shop (product, in_stock, price) VALUES ('marshmello', 10, 10);
```
+ Ok, we can write some code now, for todays lab we will use python, but you may use any programming language you like. Lets make `buy_product` funciton:
```python
import psycopg2

conn = psycopg2.connect("dbname=lab host=localhost password=admin user=admin")

price_request = "SELECT price FROM Shop WHERE product = %(product)s"
buy_decrease_balance = f"UPDATE Player SET balance = balance - ({price_request}) * %(amount)s WHERE username = %(username)s"
buy_decrease_stock = "UPDATE Shop SET in_stock = in_stock - %(amount)s WHERE product = %(product)s"


def buy_product(username, product, amount):
    with conn:
        with conn.cursor() as cur:
            obj = {"product": product, "username": username, "amount": amount}

            try:
                cur.execute(buy_decrease_balance, obj)
                if cur.rowcount != 1:
                    raise Exception("Wrong username")
            except psycopg2.errors.CheckViolation as e:
                raise Exception("Bad balance")

            try:
                cur.execute(buy_decrease_stock, obj)
                if cur.rowcount != 1:
                    raise Exception("Wrong product or out of stock")
            except psycopg2.errors.CheckViolation as e:
                raise Exception("Product is out of stock")

            conn.commit()
```
How does it works? First of all in the beginning we have import of library that works with postgresql. A bit bellow there is connection code to connect to database and templates of our queries to database. psycopg2 automatically creates, finishes and rollbacks transactions.  If no exceptions raised in block `with conn`, then transaction commited. If exception occurs, then transaction is rolled back. 

There is also `try-except` blocks insyde. Actually there is two ways of checking that there is no such player or balance is to low: you may get this data using multiple select queries (`SELECT balance FROM Player WHERE username = %s` and `SELECT price FROM Shop WHERE product = %s`) and check it in code, or you may rely on cheks in database (if constraints that we created for tables failed - psycopg2 raises `CheckViolation` exception and we may handle it) as shown in example below and check count of updated rows. 

## Homework

As a homework your task is to improve to make it also increase amount of product in players inventory. To make this you will need to add table `Inventory` which stores username, product name and amount of this product in inventory. 

There is also constraint that in inventory player can store only 100 products in total (e.g. if player have 20 marshmellows and 10 lets say bananas, then there is still possible to add 70 items). If player reached limit of items in inventory, than buy transaction should be rolled back and appropriate exception raised. 

P.S. No PL limitation in this lab, you should use Postgres (or any other relational DBMS with transactions support), but you may implement rollbacks in any PL you wish.


## SQL

```sql
CREATE TABLE Inventory (
    username TEXT REFERENCES player, 
    product  TEXT REFERENCES shop,
    amount   INT CHECK (amount >= 0),
    
    PRIMARY KEY (username, product)
);

```
