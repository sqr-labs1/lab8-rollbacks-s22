import psycopg2

conn = psycopg2.connect("dbname=lab host=localhost password=mysecretpassword user=postgres")

price_request = "SELECT price " \
                "FROM Shop " \
                "WHERE product = %(product)s"
buy_decrease_balance = f"UPDATE Player" \
                       f" SET balance = balance - ({price_request}) * %(amount)s " \
                       f"WHERE username = %(username)s"
buy_decrease_stock = "UPDATE Shop " \
                     "SET in_stock = in_stock - %(amount)s " \
                     "WHERE product = %(product)s"


buy_check_inventory_amount = "SELECT SUM(amount) <= 100 " \
                             "FROM Inventory " \
                             "WHERE username = %(username)s"

buy_increase_inventory = (
    "INSERT INTO inventory (username, product, amount) "
    "VALUES (%(username)s, %(product)s, %(amount)s) "
    "ON conflict (username, product) DO UPDATE "
    "SET amount = excluded.amount + inventory.amount "
    "RETURNING amount"
)


def buy_product(username, product, amount):
    assert amount >= 0, "Amount cannot be less than 0"

    with conn:
        with conn.cursor() as cur:
            obj = {"product": product, "username": username, "amount": amount}

            try:
                cur.execute(buy_decrease_balance, obj)
                if cur.rowcount != 1:
                    raise Exception("Wrong username")
            except psycopg2.errors.CheckViolation as e:
                raise Exception("Bad balance")

            try:
                cur.execute(buy_decrease_stock, obj)
                if cur.rowcount != 1:
                    raise Exception("Wrong product or out of stock")
            except psycopg2.errors.CheckViolation as e:
                raise Exception("Product is out of stock")

            cur.execute(buy_increase_inventory, obj)

            cur.execute(buy_check_inventory_amount, obj)
            assert cur.fetchone()[0], "Not enough space in inventory"

            conn.commit()